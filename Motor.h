/*
	Motor.h - Version 1.0
	
	Library for controlling 6 wire stepper motors
	with Arduino.
	
	Created by Marcos R. Martínez, July 31, 2013.
	Released into the public domain.
	
	Please, for bug reports or suggestions, mail me:
	adm.wirelearn@gmail.com
*/

#ifndef Motor_h
#define Motor_h

#define MODE_HS 0
#define MODE_FS 1
#define MODE_WD 2

#include "Arduino.h"

typedef void (* callbackProcedure) ();

class Motor {

	public:
		Motor(int stepsPerRevolution, int coil_a, int coil_b, int coil_c, int coil_d, int mode);
		void stepsForward(int steps);
		void stepsBackward(int steps);
		void degsForward(int degs);
		void degsBackward(int degs);
		void setRPM(int rpm);
		int getRPM();
		int getPulseMs();
		double getDegsPerStep();
		char getProcess();
		bool isForward();
		bool isBackward();
		bool isWorking();
		void onProcessChange(callbackProcedure f);
		void onRPMChange(callbackProcedure f);
		
	private:
		long _t1;
		long _t2;
		int _cur_step;
		int _coil_a;
		int _coil_b;	
		int _coil_c;
		int _coil_d;
		int _pulse_ms;
		int _rpm;
		char _process;
		double _degsPerStep;
		int _stepsPerRevolution;
		int _mode_steps;
		boolean _status[8][4];
		callbackProcedure _process_changed_callback;
		callbackProcedure _rpm_changed_callback;
		void doStep();
		void shiftForward();
		void shiftBackward();
		void setProcess(char process);
};

#endif