/* This sketch is a simple example of working with Motor lib
and lcd. The motor turns a complete revolution in both directions,
and waits half a second between revs.

Additionally, a new random speed is set at the end of every loop.
Meanwhile, LCD shows the current speed and process of the motor
Enjoy! :)

Marcos R. Martinez - adm.wirelearn@gmail.com
wirelearn.blogspot.com.es

*/

#include <Motor.h>
#include <LiquidCrystal.h>

Motor motor(48, 4, 5, 6, 7, MODE_FS);  // 48 SPR, A,B,C,D at 4,5,6,7, FULL STEP MODE
LiquidCrystal lcd(13,12,11,10,9,8);    // Initialising our LCD.

void setup(){
  
  lcd.begin(16,2);
  motor.setRPM(60);    // would be 1 RPM by default
   
  // MOST IMPORTANT PART (IMO):
  // "myPrintProcessFunction" will be executed in every "Proces" change.
  // "myPrintPrintRPMFunction" will be executed in every "RPM" change.
  // process change means when the motor STOPS, STARTS, or is being created/CONDFIGURED
  // Look at "myPrintFunction" at the end of the sketch
  motor.onProcessChange(myPrintProcessFunction);
  motor.onRPMChange(myPrintRPMFunction);
}

void loop(){
  motor.stepsForward(48);       // 48 full steps (96 half Steps for MODE_HS) are equal to a complete rev.
  delay(500);
  motor.stepsBackward(48);      // 48 full steps (96 half Steps for MODE_HS) are equal to a complete rev.
  delay(500);

  motor.setRPM(random(30,60));  // Let's set a new random speed between 30 and 60 rpm
}

void myPrintProcessFunction(){
  // This function will print "Process: A" at the first line
  // depending on the current process of the motor.
  // (S)top, (F)orward, (B)ackward, (C)onfiguring
  lcd.setCursor(0,1);
  lcd.print("Process: ");
  lcd.print(motor.getProcess());
}

void myPrintRPMFunction(){  
  // This function will print "Speed(rpm): xx" at the first line
  lcd.setCursor(0,0);
  lcd.print("Speed(rpm): ");
  lcd.print(motor.getRPM());
}

