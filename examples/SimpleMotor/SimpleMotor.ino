#include <Motor.h>

// My motor has 48 SPR. Wired to arduino's 4,5,6 and 7 pins.
// Let's set Step mode to FULL STEP
Motor motor(48, 4, 5, 6, 7, MODE_FS);

void setup(){
   motor.setRPM(60);         // Set speed to 60 R.P.M.
}

void loop(){
  motor.degsForward(360);    // 1 Rev (360 degrees) forward
  delay(500);
  motor.degsBackward(360);   // 1 Rev (360 degrees) backward
  delay(500);
}

