/*
	Motor.cpp - Version 1.0
	
	Library for controlling 6 wire stepper motors
	with Arduino

	Created by Marcos R. Martínez, July 31, 2013.
	Released into the public domain.
	
	Please, for bug reports or suggestions mail me:
	adm.wirelearn@gmail.com

	DESCRIPTION:
	
	Stepping Modes supported in this version:
	
		- MODE_WD: Wave Drive mode.
		- MODE_HS: Half-Step mode.
		- MODE_FS: Full-Step mode.
	
	NOTES:
	
	About steps <IMPORTANT>
	
		Note that operation mode affects the number of steps per
		revolution.
		
		Suppose 48-step motor:
		
			Operating in Wave drive or Half Step (MODE_WD and MODE_HS
			respectively) modes, the number of steps (output sequence
			advances) to complete a turn is 48.
		
			On the other hand, Half Step mode operation (MODE_HS) needs
			double amount of advances than the othes, because the resolution
			is twice. That's 96 steps to complete a turn.
	
	About operation modes:
	
		- Full step mode is recommended for strength applications, due to
		  its higher torque.
		
		- Half Step mode is recommended for precission applications, due to
		 its higher resolution.
		
		- Wave drive mode is the simpliest mode, and works fine at higher 
		  speeds than other modes.
		  
	About callback functions:
	
		All functions takes some CPU time. That is why I designed a callback
		based system to gather any lib info without interfere with the motor
		work in process.
		
		This can be done by registering an arduino function with the
		onProcessChange("yourFunctionHere") and onRPMChange("yourFunctionHere")
		functions.
		
*/

#include "Arduino.h"
#include "Motor.h"

const boolean STATUS_HS[8][4] =  {
							{1,0,0,0},
							{1,1,0,0},
							{0,1,0,0},
							{0,1,1,0},
							{0,0,1,0},
							{0,0,1,1},
							{0,0,0,1},
							{1,0,0,1}
							};

const boolean STATUS_FS[4][4] =  {
							{1,0,0,1},
							{1,1,0,0},
							{0,1,1,0},
							{0,0,1,1}
							};

const boolean STATUS_WD[4][4] =  {
							{1,0,0,0},
							{0,1,0,0},
							{0,0,1,0},
							{0,0,0,1}
							};
							
const int STEPS_HS = 8;
const int STEPS_FS = 4;
const int STEPS_WD = 4;

Motor::Motor(int stepsPerRevolution, int coil_a, int coil_b, int coil_c, int coil_d, int mode){
	setProcess('C');
	pinMode(coil_a,OUTPUT);
    pinMode(coil_b,OUTPUT);
    pinMode(coil_c,OUTPUT);
    pinMode(coil_d,OUTPUT);
	_coil_a = coil_a;
	_coil_b = coil_b;
	_coil_c = coil_c;
	_coil_d = coil_d;
	
	switch (mode){
		case MODE_HS:	// HALF STEP OPERATION MODE
			_mode_steps = STEPS_HS;
			memcpy(_status, STATUS_HS, sizeof(STATUS_HS));
			_stepsPerRevolution = 2 * stepsPerRevolution; // Half step = double total steps
		break;
		case MODE_FS:	// FULL STEP OPERATION MODE
			_mode_steps = STEPS_FS;
			memcpy(_status, STATUS_FS, sizeof(STATUS_FS));
			_stepsPerRevolution = stepsPerRevolution;
		break;
		case MODE_WD:	// WAVE DRIVE OPERATION MODE
			_mode_steps = STEPS_WD;
			memcpy(_status, STATUS_WD, sizeof(STATUS_WD));
			_stepsPerRevolution = stepsPerRevolution;
		break;
	}
	
	_cur_step = 0;

	_degsPerStep = 360.0 / _stepsPerRevolution;
	_rpm = 1;
	_pulse_ms = 60000 / _stepsPerRevolution;
	
	doStep(); // Positioning motor: IMPORTANT!! :)
	setProcess('S');
}

//PUBLIC

void Motor::stepsForward(int steps){
	_t1 = millis();
	setProcess('F');
	for (int i = 0 ; i < steps ; i++){
		shiftForward();
		doStep();
		_t2 = millis();
		delay(_pulse_ms - (_t2 - _t1));
		_t1 = millis();
	}
	setProcess('S');
}

void Motor::stepsBackward(int steps){
	_t1 = millis();
	setProcess('B');
	for (int i = 0 ; i < steps ; i++){
		shiftBackward();
		doStep();
		_t2 = millis();
		delay(_pulse_ms - (_t2 - _t1));
		_t1 = millis();
	}
	setProcess('S');
}

void Motor::degsForward(int degs){
	stepsForward(degs / _degsPerStep);
}

void Motor::degsBackward(int degs){
	stepsBackward(degs / _degsPerStep);
}

void Motor::setRPM(int rpm){
  if (rpm > 0)
	_pulse_ms = 60000/(_stepsPerRevolution*rpm);
  else
	_pulse_ms = -1;	// TROLLFACES FILTERING
  _rpm = rpm;
  if (_rpm_changed_callback != NULL)
	_rpm_changed_callback();	// CALL CALLBACK IF ANY
}

int Motor::getRPM(){
	return _rpm;
}
int Motor::getPulseMs(){
	return _pulse_ms;
}

double Motor::getDegsPerStep(){
	return _degsPerStep;
}

char Motor::getProcess(){
	return _process;
}

bool Motor::isForward(){
	return _process == 'F';
}

bool Motor::isBackward(){
	return _process == 'B';
}

bool Motor::isWorking(){
	return _process =! 'S'; 
}

void Motor::onProcessChange(callbackProcedure f){
	_process_changed_callback = f;
}

void Motor::onRPMChange(callbackProcedure f){
	_rpm_changed_callback = f;
}

// PRIVATE

void Motor::doStep(){
	  digitalWrite(_coil_a, _status[_cur_step][0]);
      digitalWrite(_coil_b, _status[_cur_step][1]);
      digitalWrite(_coil_c, _status[_cur_step][2]);
      digitalWrite(_coil_d, _status[_cur_step][3]);
}

void Motor::shiftForward(){
	_cur_step = (_cur_step == _mode_steps-1) ? _cur_step = 0 : _cur_step+1;
}

void Motor::shiftBackward(){
	_cur_step = (_cur_step < 1) ? _cur_step = _mode_steps-1 : _cur_step-1;
}

void Motor::setProcess(char process){
	_process = process;
	if (_process_changed_callback != NULL)
		_process_changed_callback();	// CALL CALLBACK IF ANY
}
